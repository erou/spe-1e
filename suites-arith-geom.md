## Suites arithmétiques et géométriques

### Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-1e/pdfs/cours-suites-arith-geom.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-1e/pdfs/exos-suites-arith-geom.pdf)

---

### Quelques ressources

Une *playlist* de Monsieur Monka, encore une fois, vous permettra de tout
récapituler.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=pG7DthLvKst-iYcx&amp;list=PLVUDmbpupCarbgrGmxYnfkNLlSioDg0-5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

À venir.
