## Variables aléatoires

### Fichiers de cours

+ [Activité
  d'introduction](https://erou.forge.apps.education.fr/spe-1e/pdfs/activite-variables-aleatoires.pdf)
+ [Cours](https://erou.forge.apps.education.fr/spe-1e/pdfs/cours-variables-aleatoires.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-1e/pdfs/exos-variables-aleatoires.pdf)

---

### Quelques ressources

Vous ré-écouterez bien un peu Yvan Monka ?

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=-npqqwtlTP37u5x2&amp;list=PLVUDmbpupCapvPJP6PMpsUlFtlx93mI0l" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

Une vidéo sur la *loi des grands nombres*, qui permet de comprendre un peu
plus l'intérêt de l'espérance.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/HRnYFpdR8WM?si=laTxEoMuLxLlmlLQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
