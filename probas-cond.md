## Probabilités conditionnelles

### Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-1e/pdfs/cours-probas-cond.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-1e/pdfs/exos-probas-cond.pdf)
+ [Exercices de M.
  Bonnardot](https://erou.forge.apps.education.fr/spe-1e/pdfs/bonnardot-probas-cond.pdf)

---

### Quelques ressources

Évidemment, Yvan Monka a fait un travail remarquablement complet sur le sujet.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=Jg5eZbshJ0u9MeH1&amp;list=PLVUDmbpupCapoStVETZ2x6iy0vCua0HvK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

Une vidéo autour d'un problème célèbre, que l'on peut comprendre grâce à la
théorie des probabilités !

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/ZPSH6l_darY?si=kxuv2r9fw8XaoeN9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
