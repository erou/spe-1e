## Suites - Généralités

### Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-1e/pdfs/cours-suites.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-1e/pdfs/exos-suites.pdf)
+ [Activité
  d'introduction](https://erou.forge.apps.education.fr/spe-1e/pdfs/activite-suites.pdf)
+ [Exercices de M.
  Bonnardot](https://erou.forge.apps.education.fr/spe-1e/pdfs/bonnardot-suites.pdf)


---

### Quelques ressources

Une *playlist* concoctée par Monsieur Yvan Monka pourra vous permettre de tout
connaître sur ce chapitre.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=B2Ui8s4W6DcYPmDC&amp;list=PLVUDmbpupCaoqExMkHrhYvWi4dHnApgG_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

Certaines suites sont définies de manière étonnante, mais on peut quand même
essayer de les étudier !

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/IsKBRj6_VSs?si=3CBviCO00mTmhJes" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
