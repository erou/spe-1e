## Dérivation

### Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-1e/pdfs/cours-derivation.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-1e/pdfs/exos-derivation.pdf)
+ [Exercices de M.
  Bonnardot](https://erou.forge.apps.education.fr/spe-1e/pdfs/bonnardot-derivation.pdf)

---

### Quelques ressources

Le travail de monsieur Yvan Monka, toujours très complet, pourra
éventuellement vous être utile.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=0sM6EiaGyQQc-9VK&amp;list=PLVUDmbpupCar85S0V4y-Flv426LmtZ2YZ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/NsYxAZPmWho?si=qrTydIcRfOEBR5e9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
