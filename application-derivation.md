## Applications de la dérivation

### Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-1e/pdfs/cours-application-derivation.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-1e/pdfs/exos-application-derivation.pdf)

---

### Quelques ressources

La *playlist* du chapitre *Dérivation* contenait déjà tout ce qu'il faut.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=0sM6EiaGyQQc-9VK&amp;list=PLVUDmbpupCar85S0V4y-Flv426LmtZ2YZ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

Une vidéo sur l'histoire de la dérivation, maintenant que vous connaissez tout !

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/RLEE-iSBimc?si=qpFRtnGEcqnFZnUl" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>
