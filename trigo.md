## Trigonométrie

### Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-1e/pdfs/cours-trigo.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-1e/pdfs/exos-trigo.pdf)

---

### Quelques ressources

Une fois n'est pas coutume : voici une *playlist* d'Yvan Monka, qui traite le
sujet de A à Z.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=dnPgCkmkYMikznNX&amp;list=PLVUDmbpupCarfQSOriLQlwiLITHyzTI60" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

Un article en anglais sur d'autres fonctions trigonométriques ~beaucoup~ plus
exotiques.
- [10 Secret Trig Functions Your Math Teachers Never Taught
  You](https://blogs.scientificamerican.com/roots-of-unity/10-secret-trig-functions-your-math-teachers-never-taught-you/)
