## Second degré

### Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-1e/pdfs/cours-second-degre.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-1e/pdfs/exos-second-degre.pdf)
+ [Éléments de correction](https://erou.forge.apps.education.fr/spe-1e/pdfs/corrections-second-degre.pdf)
+ [Exercices de M. Bonnardot](https://erou.forge.apps.education.fr/spe-1e/pdfs/bonnardot-second-degre.pdf)

---

### Quelques ressources

Une *playlist* (très complète) d'Yvan Monka comprenant plus de 40 vidéos, pour tout
savoir sur le second degré.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=h4x63HpilY4li1eA&amp;list=PLVUDmbpupCaqilBbriQYMVjn0BRBCgwdH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>

---

### Pour aller plus loin

L'ensemble des nombres complexes (noté $$\mathbb{C}$$) permet (entre autre)
de résoudre *absolument toutes* les équations du second degré à coefficients
dans $$\mathbb{R}$$, même celles dont le discriminant est strictement négatif.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/M6o5CRYfNxA?si=TNr58MEDZuOxW-T5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
</div>
