## Fonction exponentielle

### Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/spe-1e/pdfs/cours-expo.pdf)
+ [Exercices](https://erou.forge.apps.education.fr/spe-1e/pdfs/exos-expo.pdf)

---

### Quelques ressources

Une *playlist* de Monsieur Monka, au cas où vous ne le connaitriez pas.
 
<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=oLAdGK-geQt0Wslp&amp;list=PLVUDmbpupCaofwlm6oHUQa0YArPzMZ4M7" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>
